#!/bin/bash
set -euo pipefail

out_dir=data
mkdir -p extracted "$out_dir"
tar -C extracted/ -xf ted_talks.tar.gz

for dset in train dev test; do
    talk="extracted/all_talks_$dset.tsv"
    prefix="$out_dir/$dset."
    languages=( $(head -1 "$talk" | sed "s|^|$prefix|; s|\t|\t$prefix|g") )
    tail -n+2 "$talk" | ./unpaste "${languages[@]}"
done
