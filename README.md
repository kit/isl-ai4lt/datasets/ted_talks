# Installing DVC (with SSH backend)
With pip:
```
pip install dvc[ssh]
```
with conda-forge:
```
conda install -c conda-forge dvc dvc-ssh
```

# Pull data from remote
```
git clone https://git.scc.kit.edu/isl/datasets/ted_talks.git
cd ted_talks/
dvc pull
```

# Reproduce data from scratch
```
git clone https://git.scc.kit.edu/isl/datasets/ted_talks.git
cd ted_talks/
dvc update ted_talks.tar.gz
dvc repro
```
